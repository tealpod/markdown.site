##### Markdown语法教程 (Markdown syntax tutorial)

- [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax/ "Markdown Syntax")
- [Mastering Markdown](https://guides.github.com/features/mastering-markdown/ "Mastering Markdown")
- [Markdown Basics](https://help.github.com/articles/markdown-basics/ "Markdown Basics")
- [GitHub Flavored Markdown](https://help.github.com/articles/github-flavored-markdown/ "GitHub Flavored Markdown")
- [Markdown 语法说明（简体中文）](http://www.markdown.cn/ "Markdown 语法说明（简体中文）")
- [Markdown 語法說明（繁體中文）](http://markdown.tw/ "Markdown 語法說明（繁體中文）")

##### 键盘快捷键 (Keyboard shortcuts)

> If Editor.md code editor is on focus, you can use keyboard shortcuts.
    
| Keyboard shortcuts (键盘快捷键)                 |   Description                            | 说明                                        |
| :---------------------------------------------- |:--------------------------------- | :------------------------------------------------- |
| F9                                              | Switch watch/unwatch              | 切换实时预览                                  |
| F10                                             | Full preview HTML (Press Shift + ESC exit) | 全屏HTML预览(按 Shift + ESC 退出) |
| F11                                             | Switch fullscreen (Press ESC exit) | 切换全屏状态                      | 
| Ctrl + 1~6 / Command + 1~6                      | Insert heading 1~6                 | 插入标题1~6                       | 
| Ctrl + A / Command + A                          | Select all                         | 全选                              | 
| Ctrl + B / Command + B                          | Insert bold                        | 插入粗体                          | 
| Ctrl + D / Command + D                          | Insert datetime                    | 插入日期时间                      | 
| Ctrl + E / Command + E                          | Insert &#58;emoji&#58;             | 插入Emoji符号                     | 
| Ctrl + F / Command + F                          | Start searching                    | 查找/搜索                         | 
| Ctrl + G / Command + G                          | Find next search results           | 切换到下一个搜索结果项            | 
| Ctrl + H / Command + H                          | Insert horizontal rule             | 插入水平线                        | 
| Ctrl + I / Command + I                          | Insert italic                      | 插入斜体                          | 
| Ctrl + K / Command + K                          | Insert inline code                 | 插入行内代码                      | 
| Ctrl + L / Command + L                          | Insert link                        | 插入链接                          |
| Ctrl + U / Command + U                          | Insert unordered list              | 插入无序列表                      | 
| Ctrl + Q                                        | Switch code fold                   | 代码折叠切换                      | 
| Ctrl + Z / Command + Z                          | Undo                               | 撤销                              | 
| Ctrl + Y / Command + Y                          | Redo                               | 重做                              | 
| Ctrl + Shift + A                                | Insert &#64;link                   | 插入@链接                         | 
| Ctrl + Shift + C                                | Insert inline code                 | 插入行内代码                      | 
| Ctrl + Shift + E                                | Open emoji dialog                  | 打开插入Emoji表情对话框           | 
| Ctrl + Shift + F / Command + Option + F         | Replace                            | 替换                              | 
| Ctrl + Shift + G / Shift + Command + G          | Find previous search results       | 切换到上一个搜索结果项            | 
| Ctrl + Shift + H                                | Open HTML Entities dialog          | 打开HTML实体字符对话框            | 
| Ctrl + Shift + I                                | Insert image &#33;[]&#40;&#41;     | 插入图片                          | 
| Ctrl + Shift + K                                | Insert TeX(KaTeX) symbol &#36;&#36;TeX&#36;&#36;   | 插入TeX(KaTeX)公式符号            | 
| Ctrl + Shift + L                                | Open link dialog                   | 打开插入链接对话框                | 
| Ctrl + Shift + O                                | Insert ordered list                | 插入有序列表                      | 
| Ctrl + Shift + P                                | Open Preformatted text dialog      | 打开插入PRE对话框                 | 
| Ctrl + Shift + Q                                | Insert blockquotes                 | 插入引用                          | 
| Ctrl + Shift + R / Shift + Command + Option + F | Replace all                        | 全部替换                          | 
| Ctrl + Shift + S                                | Insert strikethrough               | 插入删除线                        | 
| Ctrl + Shift + T                                | Open table dialog                  | 打开插入表格对话框                | 
| Ctrl + Shift + U                                | Selection text convert to uppercase| 将所选文字转成大写                | 
| Shift + Alt + C                                 | Insert code blocks (```)           | 插入```代码                       | 
| Shift + Alt + H                                 | Open help dialog                   | 打开使用帮助对话框                | 
| Shift + Alt + L                                 | Selection text convert to lowercase| 将所选文本转成小写                | 
| Shift + Alt + P                                 | Insert page break                  | 插入分页符                        | 
| Alt + L                                         | Selection text convert to lowercase | 将所选文本转成小写                | 
| Shift + Alt + U                                 | Selection words first letter convert to Uppercase  | 将所选的每个单词的首字母转成大写  | 
| Ctrl + Shift + Alt + C                          | Open code blocks dialog            | 打开插入代码块对话框层            | 
| Ctrl + Shift + Alt + I                          | Open image dialog                  | 打开插入图片对话框层              | 
| Ctrl + Shift + Alt + U                          | Selection text first letter convert to uppercase   | 将所选文本的第一个首字母转成大写  |
| Ctrl + Alt + G                                  | Goto line                          | 跳转到指定的行                    | 


##### Emoji表情参考 (Emoji reference)

- [Github emoji](http://www.emoji-cheat-sheet.com/ "Github emoji")
- [Twitter Emoji \(Twemoji\)](http://twitter.github.io/twemoji/preview.html "Twitter Emoji \(Twemoji\)")
- [FontAwesome icons emoji](http://fortawesome.github.io/Font-Awesome/icons/ "FontAwesome icons emoji")

##### 流程图参考 (Flowchart reference)

[http://adrai.github.io/flowchart.js/](http://adrai.github.io/flowchart.js/)

##### 时序图参考 (SequenceDiagram reference)

[http://bramp.github.io/js-sequence-diagrams/](http://bramp.github.io/js-sequence-diagrams/)

##### TeX/LaTeX reference

[http://meta.wikimedia.org/wiki/Help:Formula](http://meta.wikimedia.org/wiki/Help:Formula)
