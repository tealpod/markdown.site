# Online Markdown Editor

Free Online Markdown Editor.

#### Features
 - Create/Edit markdown files online.
 - You can save your files as .md (markdown) or .HTML(beta) files.
 - Easy to remmember domain name :eyes: :thumbsup: :relieved:
 
Planning to add custom styles for html files(let me know if you have  suggestions). 
[@prakis](https://twitter.com/prakis)

-----
### Cheatsheet 


# Heading 1
## Heading 2
### Heading 3
#### Heading 4
##### Heading 5
###### Heading 6

>  Blockquotes

[Editor.md](http://editor.md.ipandao.com/)

#### Tables

| Left-Aligned  | Center Aligned  | Right Aligned |
| :------------ |:---------------:| -----:|
| col 3 is      | some wordy text | $1600 |
| col 2 is      | centered        |   $12 |
| zebra stripes | are neat        |    $1 |

#### Code
```javascript
function test() {
	console.log("Hello world!");
}
```
#### HTML codes

```html
<!DOCTYPE html>
<html>
    <head>
        <mate charest="utf-8" />
        <meta name="keywords" content="Editor.md, Markdown, Editor" />
        <title>Hello world!</title>
    </head>
    <body>
        <h1 class="text-xxl">Hello world!</h1>
        <p class="text-green">Plain text</p>
    </body>
</html>
```

### Images

Image:

![](https://picsum.photos/id/1018/536/354)


### Lists

#### Unordered Lists (-)
                
- Google
- Bing
- Baidu

#### Unordered Lists (+)
                
+ Google
+ Bing
    + Maps
    + Image
    + Video
+ Baidu
    * Maps
    * Image
    * Video

#### GFM task list

- [x] GFM task list 1
- [x] GFM task list 2
- [ ] GFM task list 3
    - [ ] GFM task list 3-1
    - [ ] GFM task list 3-2
- [ ] GFM task list 4
    - [ ] GFM task list 4-1
    - [ ] GFM task list 4-2

#### HTML Entities Codes

&copy; &  &uml; &trade; &iexcl; &pound;
&amp; &lt; &gt; &yen; &euro; &reg; &plusmn; &para; &sect; &brvbar; &macr; &laquo; &middot; 

X&sup2; Y&sup3; &frac34; &frac14;  &times;  &divide;   &raquo;

18&ordm;C  &quot;  &apos;

[========]

#### [Github Emoji](https://gist.github.com/rxaviers/7360908 "Github Emoji")
:+1: :octocat: :honeybee: :mouse: :lock: :unlock: :paperclip: :blue_book: :one: :two: :three:

#### [FontAwesome icons](https://fontawesome.com/v4.7.0/icons/ "FontAwesome icons") 
:fa-star: :fa-gear: :fa-download: :fa-check: :fa-times: :fa-cog: :fa-play: :fa-sitemap: :fa-bell-o: :fa-female: :fa-male: :fa-bitbucket: :fa-windows: :fa-android: :fa-linux: :fa-youtube: :fa-youtube-square: :fa-dribbble: :fa-google: :fa-reddit: :fa-reddit-square: :fa-university: :fa-wordpress: :fa-slack: :fa-recycle: :fa-behance-square: :fa-behance: :fa-steam: :fa-jsfiddle: :fa-codepen: :fa-git: :fa-paypal:

#### [Twemoji](https://twemoji.twitter.com/)
:tw-1f335: :tw-1f36d: :tw-1f346: :tw-1f340: :tw-1f341: :tw-1f37a: :tw-1f382: :tw-1f3c1: :tw-1f3b9: :tw-1f3b8: :tw-1f3b7: :tw-1f3c6: :tw-1f425: :tw-1f424: :tw-1f41d: :tw-1f418: :tw-1f4cf: :tw-a9: :tw-ae: :tw-2709: :tw-270f:
----
 
#### Escape 反斜杠

\*literal asterisks\*

[========]
            
### TeX(KaTeX) 科学公式

$$E=mc^2$$

$$x > y$$

$$\(\sqrt{3x-1}+(1+x)^2\)$$
                    
$$\sin(\alpha)^{\theta}=\sum_{i=0}^{n}(x^i + \cos(f))$$


```math
\displaystyle
\left( \sum\_{k=1}^n a\_k b\_k \right)^2
\leq
\left( \sum\_{k=1}^n a\_k^2 \right)
\left( \sum\_{k=1}^n b\_k^2 \right)
```

```katex
\displaystyle 
    \frac{1}{
        \Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{
        \frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {
        1+\frac{e^{-6\pi}}
        {1+\frac{e^{-8\pi}}
         {1+\cdots} }
        } 
    }
```

```latex
f(x) = \int_{-\infty}^\infty
    \hat f(\xi)\,e^{2 \pi i \xi x}
    \,d\xi
```

### Page break 分页符 

> Print Test: Ctrl + P

[========]

### Flowchart 绘制流程图

```flow
st=>start: Doubt
op=>operation: Question
cond=>condition:  Yes or No?
e=>end: Result

st->op->cond
cond(yes)->e
cond(no)->op
```

[========]
                    
### Sequence Diagram 绘制序列图
                    
```seq
Andrew->Lee: Says Hello 
Note right of Lee: Lee thinks\nabout it 
Lee-->Andrew: Hi, How are you? 
Andrew->>Lee: I am good thanks!
```

------------



- This site developed using [Editor.md](http://editor.md.ipandao.com/)
- Code on [Bitbucket](https://bitbucket.org) :fa-bitbucket:
- Hosted on [Netlify](https://netlify.com/) [![Netlify Status](https://api.netlify.com/api/v1/badges/3c3be5f2-129c-49e4-94c3-dd658888232d/deploy-status)](https://app.netlify.com/sites/markdownsite/deploys)

Goodluck,
[@prakis](https://twitter.com/prakis)






